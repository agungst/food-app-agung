//
//  ApiHelper.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation
import Moya

enum ApiHelper: BaseApi {    
    
    case menu
    case banner
    
    var baseURL: URL {
        return URL(string: "https://5fa265e8ba0736001613b92c.mockapi.io")!
    }
    
    var path: String {
        switch self {
        case .menu:
            return "/menu"
        case .banner:
            return "/banner"
        }
    }
}
