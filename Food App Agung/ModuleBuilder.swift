//
//  ModuleBuilder.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation

class ModuleBuilder {
    
    static let shared = ModuleBuilder()
    
    private init() {}
    
    func createMenuViewController() -> MenuViewController {
        let view = MenuViewController(nibName: String(describing: MenuViewController.self), bundle: nil)
        let interactor = MenuInteractor()
        let router = MenuRouter()
        let presenter = MenuPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.output = presenter
        router.viewController = view
        return view
    }

    func createCartViewController(menus: [Menu], delegate: MenuPresenterProtocol?) -> CartViewController {
        let view = CartViewController(nibName: String(describing: CartViewController.self), bundle: nil)
        let interactor = CartInteractor()
        let router = CartRouter()
        let presenter = CartPresenter(interface: view, interactor: interactor, router: router)
        presenter.menus = menus
        router.delegate = delegate
        view.presenter = presenter
        interactor.output = presenter
        router.viewController = view
        return view
    }

}
