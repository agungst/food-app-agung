//
//  NetworkManager.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation
import Moya

class NetworkManager<T: BaseApi> {
    
    func api() -> MoyaProvider<T> {
        let provider = MoyaProvider<T>(plugins: [NetworkLoggerPlugin()])
        return provider
    }
}

protocol BaseApi: TargetType {
    
    var baseURL: URL {
        get
    }
    
    var path: String {
        get
    }
    
    var method: Moya.Method {
        get
    }
    
    var sampleData: Data {
        get
    }
    
    var task: Task {
        get
    }
    
    var headers: [String: String]? {
        get
    }
    
}

extension TargetType {

    var sampleData: Data {
        return Data()
    }

    var baseURL: URL {
        fatalError()
    }

    var path: String {
        return ""
    }

    var method: Moya.Method {
        return .get
    }

    var parameters: [String: Any] {
        return [:]
    }

    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    var task: Task {
        return .requestParameters(parameters: parameters, encoding: parameterEncoding)
    }

    var headers: [String: String]? {
        return [:]
    }

}
