//
//  MenuPresenter.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation

class MenuPresenter: MenuPresenterProtocol {
    
    weak private var view: MenuViewProtocol?
    private let interactor: MenuInteractorInputProtocol?
    private let router: MenuRouterProtocol?

    var menuEntity: MenuEntity?
    var bannerEntity: BannerEntity?
    var menuTitles: [String] = []
    var cart: [Menu] = []
    
    init(interface: MenuViewProtocol, interactor: MenuInteractorInputProtocol?, router: MenuRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    // MARK: PresenterProtocol

    func viewDidLoad() {
        view?.showProgressDialog(show: true)
        interactor?.fetchMenu()
        interactor?.fetchBanner()
    }
    
    func addToCart(menu: Menu) {
        if !cart.contains(where: {$0 === menu}) {
            cart.append(menu)
            view?.updateCart()
        }
    }
    
    func goToCart() {
        router?.goToCart(menus: cart, delegate: self)
    }
    
    func updateCart(menus: [Menu]) {
        cart = menus
    }

}

extension MenuPresenter: MenuInteractorOutputProtocol {

    // MARK: InteractorOutputProtocol

    func requestMenuSuccess(metadata: MenuEntity) {
        menuEntity = metadata
        metadata.data.forEach { (menu) in
            if !menuTitles.contains(menu.type) { menuTitles.append(menu.type) }
        }
        view?.showProgressDialog(show: false)
        view?.populateMenu()
    }

    func requestBannerSuccess(metadata: BannerEntity) {
        bannerEntity = metadata
        view?.populateBanner()
    }
    
    func requestDidFailed(message: String) {
        view?.showProgressDialog(show: false)
        view?.showErrorMessage(message: message)
    }

}
