//
//  MenuRouter.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

class MenuRouter: MenuRouterProtocol {
    
    weak var viewController: MenuViewController?

     // MARK: RouterProtocol
    func goToCart(menus: [Menu], delegate: MenuPresenterProtocol?) {
        let vc = ModuleBuilder.shared.createCartViewController(menus: menus, delegate: delegate)
        vc.modalPresentationStyle = .fullScreen
        viewController?.present(vc, animated: true)
    }
    
}
