//
//  MenuInteractor.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation
import SwiftyJSON

class MenuInteractor: MenuInteractorInputProtocol {
    
    weak var output: MenuInteractorOutputProtocol?
    let provider = NetworkManager<ApiHelper>()
    
    func fetchMenu() {
        provider.api().request(.menu) { result in
            switch result {
            case .success(let response):
                if let array = JSON(response.data).arrayObject, let metadata = MenuEntity(JSON: ["data": array]) {
                    self.output?.requestMenuSuccess(metadata: metadata)
                }
            case .failure(let err):
                self.output?.requestDidFailed(message: err.errorDescription ?? "")
            }
        }
    }
    
    func fetchBanner() {
        provider.api().request(.banner) { result in
            switch result {
            case .success(let response):
                if let array = JSON(response.data).arrayObject, let metadata = BannerEntity(JSON: ["data": array]) {
                    self.output?.requestBannerSuccess(metadata: metadata)
                }
            case .failure(let err):
                self.output?.requestDidFailed(message: err.errorDescription ?? "")
            }
        }
    }
    
}
