//
//  MenuViewController.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MaterialComponents.MaterialButtons
import SnapKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bannerColView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var titleColView: UICollectionView!
    @IBOutlet weak var menuColView: UICollectionView!
    @IBOutlet weak var menuColViewCons: NSLayoutConstraint!
    
    let disposeBag = DisposeBag()
    let banners: PublishSubject<[Banner]> = PublishSubject()
    let menuTitles: PublishSubject<[String]> = PublishSubject()
    let selectedMenuTitle: PublishSubject<String> = PublishSubject()
    let menus: PublishSubject<[Menu]> = PublishSubject()
    let cartCount: PublishSubject<Int> = PublishSubject()

    var presenter: MenuPresenterProtocol?

    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateCart()
    }

    // MARK: - publics
    // Add public functions here

    // MARK: - IBActions
    // Add IBActions in this area

    // MARK: - Privates
    private func setupViews() {
        scrollView.contentInsetAdjustmentBehavior = .never
        setupBanner()
        setupMenuTitle()
        setupMenu()
        setupCartButton()
    }
    
    private func setupBanner() {
        bannerColView.register(UINib(nibName: String(describing: BannerCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: BannerCollectionViewCell.self))
        
        let flowLayout = bannerColView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.itemSize = bannerColView.frame.size
        bannerColView.setCollectionViewLayout(flowLayout, animated: false)
        
        banners.bind(to: bannerColView.rx.items(cellIdentifier: String(describing: BannerCollectionViewCell.self), cellType: BannerCollectionViewCell.self)) { (index, banner, cell) in
            cell.banner = banner
        }.disposed(by: disposeBag)
        
        banners.subscribe(onNext: { (banners) in
            self.pageControl.numberOfPages = banners.count
            self.pageControl.currentPage = 0
        }).disposed(by: disposeBag)
        
        bannerColView.rx.willDisplayCell.subscribe(onNext: { (cell, indexPath) in
            self.pageControl.currentPage = indexPath.item
        }).disposed(by: disposeBag)
        
    }
    
    private func setupMenuTitle() {
        titleColView.register(UINib(nibName: String(describing: MenuTitleCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: MenuTitleCollectionViewCell.self))
        
        menuTitles.bind(to: titleColView.rx.items(cellIdentifier: String(describing: MenuTitleCollectionViewCell.self), cellType: MenuTitleCollectionViewCell.self)) { (index, title, cell) in
            cell.label.text = title
        }.disposed(by: disposeBag)
        
        titleColView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            let cell = self.titleColView.cellForItem(at: indexPath) as! MenuTitleCollectionViewCell
            cell.label.alpha = 1
            self.selectedMenuTitle.onNext(cell.label.text!)
        }).disposed(by: disposeBag)
        
        titleColView.rx.itemDeselected.subscribe(onNext: { (indexPath) in
            let cell = self.titleColView.cellForItem(at: indexPath) as! MenuTitleCollectionViewCell
            cell.label.alpha = 0.1
        }).disposed(by: disposeBag)
        
    }
    
    private func setupMenu() {
        menuColView.register(UINib(nibName: String(describing: MenuCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: MenuCollectionViewCell.self))
        
        let flowLayout = menuColView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.itemSize = CGSize(width: menuColView.frame.size.width, height: menuColView.frame.size.width * 1.15)
        menuColView.setCollectionViewLayout(flowLayout, animated: false)
        
        menus.bind(to: menuColView.rx.items(cellIdentifier: String(describing: MenuCollectionViewCell.self), cellType: MenuCollectionViewCell.self)) { (index, menu, cell) in
            cell.menu = menu
            cell.delegate = self
        }.disposed(by: disposeBag)
        
        menus.subscribe(onNext: { (menus) in
            UIView.animate(withDuration: 0.5) {
                self.menuColViewCons.constant = (self.menuColView.frame.size.width * 1.15) * menus.count.cgFloat
                self.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
        
        selectedMenuTitle.subscribe(onNext: { (title) in
            if let menus = self.presenter?.menuEntity?.data.filter({ $0.type == title }) {
                self.menus.onNext(menus)
            }
        }).disposed(by: disposeBag)

    }
    
    private func setupCartButton() {
        let fab = MDCFloatingButton()
        fab.minimumSize = CGSize(width: 64, height: 64)
        fab.tintColor = .black
        fab.backgroundColor = .white
        fab.setImageForAllStates(UIImage(named: "baseline_shopping_cart_white_24pt")!)
        view.addSubview(fab)
        fab.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-48)
        }
        
        let badgeView = UIView()
        badgeView.backgroundColor = UIColor.Material.green
        badgeView.cornerRadius = 10
        view.addSubview(badgeView)
        badgeView.snp.makeConstraints { (make) in
            make.width.height.equalTo(20)
            make.trailing.equalTo(fab.snp.trailing)
            make.top.equalTo(fab.snp.top)
        }
        
        let badgeLabel = UILabel(text: "0")
        badgeLabel.textColor = .white
        badgeLabel.font = UIFont.systemFont(ofSize: 12)
        badgeView.addSubview(badgeLabel)
        badgeLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        cartCount.subscribe(onNext: { (count) in
            badgeView.isHidden = count < 1
            badgeLabel.text = count.string
        }).disposed(by: disposeBag)
        
        fab.rx.tap.subscribe(onNext: { (_) in
            self.presenter?.goToCart()
        }).disposed(by: disposeBag)

    }
    
}

// MARK: - View Protocol
extension MenuViewController: MenuViewProtocol {
    func populateMenu() {
        guard let titles = presenter?.menuTitles else { return }
        menuTitles.onNext(titles)
        selectedMenuTitle.onNext(titles.first!)
        if let cell = titleColView.cellForItem(at: IndexPath(item: 0, section: 0)) as? MenuTitleCollectionViewCell {
            titleColView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
            cell.label.alpha = 1
        }
    }

    func populateBanner() {
        guard let data = presenter?.bannerEntity?.data else { return }
        banners.onNext(data)
    }
    
    func updateCart() {
        guard let cartCount = presenter?.cart.count else { return }
        self.cartCount.onNext(cartCount)
    }
    
    func showProgressDialog(show: Bool) {
        DispatchQueue.main.async {
            
        }
    }

    func showAlertMessage(title: String, message: String) {
        DispatchQueue.main.async {
            
        }
    }

    func showErrorMessage(message: String) {
        DispatchQueue.main.async {
            
        }
    }
}

extension MenuViewController: MenuCollectionViewCellDelegate {
    
    func notifyMenu(menu: Menu) {
        presenter?.addToCart(menu: menu)
    }
    
}
