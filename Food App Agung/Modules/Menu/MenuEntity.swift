//
//  MenuEntity.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation
import ObjectMapper

class MenuEntity: Mappable, Codable {
    
    var data: [Menu] = []

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
    
}

class Menu: Mappable, Codable {
    
    var id = ""
    var name = ""
    var description = ""
    var image = ""
    var price = ""
    var type = ""

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        image <- map["image"]
        price <- map["price"]
        type <- map["type"]
    }
    
}

class BannerEntity: Mappable, Codable {
    
    var data: [Banner] = []

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
    
}

class Banner: Mappable, Codable {
    
    var id = ""
    var image = ""

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        image <- map["image"]
    }
    
}
