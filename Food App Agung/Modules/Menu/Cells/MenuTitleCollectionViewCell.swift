//
//  MenuTitleCollectionViewCell.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 06/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit

class MenuTitleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    
    var type: String? {
        didSet {
            label.text = type
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
