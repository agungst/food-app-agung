//
//  MenuCollectionViewCell.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 06/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol MenuCollectionViewCellDelegate {
    func notifyMenu(menu: Menu)
}

class MenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    let disposeBag = DisposeBag()
    
    var menu: Menu? {
        didSet {
            guard let menu = menu else { return }
            if let imageUrl = URL(string: menu.image) {
                imageView.kf.setImage(with: imageUrl)
            }
            nameLabel.text = menu.name
            descLabel.text = menu.description
            button.setTitle("USD \(menu.price)", for: .normal)
        }
    }
    var delegate: MenuCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.kf.indicatorType = .activity
        button.cornerRadius = button.frame.size.height / 2
        button.setTitle("Added +1", for: .highlighted)
        button.setTitleColor(.white, for: .normal)
        
        button.rx.tap.subscribe(onNext: { (_) in
            guard let menu = self.menu else { return }
            self.delegate?.notifyMenu(menu: menu)
        }).disposed(by: disposeBag)
    }

}
