//
//  BannerCollectionViewCell.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 06/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit
import Kingfisher

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    var banner: Banner? {
        didSet {
            guard let bannerUrlString = banner?.image, let bannerUrl = URL(string: bannerUrlString) else { return }
            
            imageView.kf.setImage(with: bannerUrl)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.kf.indicatorType = .activity
    }

}
