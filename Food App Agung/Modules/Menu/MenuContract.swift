//
//  MenuContract.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 04/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

protocol MenuRouterProtocol: class {
    func goToCart(menus: [Menu], delegate: MenuPresenterProtocol?)
}

protocol MenuPresenterProtocol: class {
    
    var menuEntity: MenuEntity? { get set }
    var bannerEntity: BannerEntity? { get set }
    var menuTitles: [String] { get set }
    var cart: [Menu] { get set }

    func viewDidLoad()
    func addToCart(menu: Menu)
    func goToCart()
    func updateCart(menus: [Menu])

}

protocol MenuInteractorOutputProtocol: class {
    
    func requestMenuSuccess(metadata: MenuEntity)
    func requestBannerSuccess(metadata: BannerEntity)
    func requestDidFailed(message: String)
}

protocol MenuInteractorInputProtocol: class {
    
    var output: MenuInteractorOutputProtocol? { get set }

    func fetchMenu()
    func fetchBanner()

}

protocol MenuViewProtocol: class {
    
    // Presenter -> ViewController
    func populateMenu()
    func populateBanner()
    func updateCart()
    func showProgressDialog(show: Bool)
    func showAlertMessage(title: String, message: String)
    func showErrorMessage(message: String)
    
}
