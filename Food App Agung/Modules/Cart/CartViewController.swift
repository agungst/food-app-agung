//
//  CartViewController.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 08/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CartViewController: UIViewController {
    
    @IBOutlet weak var backStackView: UIStackView!
    @IBOutlet weak var colView: UICollectionView!
    @IBOutlet weak var colViewCons: NSLayoutConstraint!
    @IBOutlet weak var valueLabel: UILabel!
    
    let disposeBag = DisposeBag()
    let menus: PublishSubject<[Menu]> = PublishSubject()

    var presenter: CartPresenterProtocol?
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        presenter?.viewDidLoad()
    }

    // MARK: - publics
    // Add public functions here

    // MARK: - IBActions
    // Add IBActions in this area

    // MARK: - Privates
    private func setupViews() {
        setupBack()
        setupCart()
    }
    
    private func setupBack() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(backToMenu))
        backStackView.isUserInteractionEnabled = true
        backStackView.addGestureRecognizer(tap)
    }
    
    @objc private func backToMenu() {
        presenter?.backToMenu()
    }
    
    private func setupCart() {
        colView.register(UINib(nibName: String(describing: CartCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CartCollectionViewCell.self))
        
        let flowLayout = colView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.itemSize = CGSize(width: colView.frame.size.width, height: 100)
        colView.setCollectionViewLayout(flowLayout, animated: false)
        
        menus.bind(to: colView.rx.items(cellIdentifier: String(describing: CartCollectionViewCell.self), cellType: CartCollectionViewCell.self)) { (index, menu, cell) in
            cell.menu = menu
            cell.delegate = self
        }.disposed(by: disposeBag)
        
        menus.subscribe(onNext: { (menus) in
            UIView.animate(withDuration: 0.5) {
                self.colViewCons.constant = CGFloat(100 * menus.count)
                self.valueLabel.text = "USD \(menus.compactMap({$0.price.float() ?? 0}).reduce(0, +))"
                self.view.layoutIfNeeded()
            }
        }).disposed(by: disposeBag)
        
        menus.onNext(presenter!.menus)
    }
}

// MARK: - View Protocol
extension CartViewController: CartViewProtocol {
    func populateData() {
        // TODO: Populate data
    }

    func showProgressDialog(show: Bool) {
        DispatchQueue.main.async {
            
        }
    }

    func showAlertMessage(title: String, message: String) {
        DispatchQueue.main.async {
            
        }
    }

    func showErrorMessage(message: String) {
        DispatchQueue.main.async {
            
        }
    }
}

extension CartViewController: CartCollectionViewCellDelegate {
    
    func removeItem(menu: Menu) {
        presenter?.menus.removeAll(where: {$0 === menu})
        menus.onNext(presenter!.menus)
    }
    
}

