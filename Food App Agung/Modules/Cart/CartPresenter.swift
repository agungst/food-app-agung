//
//  CartPresenter.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 08/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation

class CartPresenter: CartPresenterProtocol {
    
    weak private var view: CartViewProtocol?
    private let interactor: CartInteractorInputProtocol?
    private let router: CartRouterProtocol?

    var menus: [Menu] = []
    
    init(interface: CartViewProtocol, interactor: CartInteractorInputProtocol?, router: CartRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    // MARK: PresenterProtocol

    func viewDidLoad() {
        // TODO: Add on view loaded
        view?.showProgressDialog(show: true)
        interactor?.fetchData()
    }
    
    func backToMenu() {
        router?.backToMenu(menus: menus)
    }

}

extension CartPresenter: CartInteractorOutputProtocol {

    // MARK: InteractorOutputProtocol

    func requestDidSuccess() {
        view?.showProgressDialog(show: false)
        view?.populateData()
    }

    func requestDidFailed(message: String) {
        view?.showProgressDialog(show: false)
        view?.showErrorMessage(message: message)
    }

}
