//
//  CartCollectionViewCell.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 08/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol CartCollectionViewCellDelegate {
    func removeItem(menu: Menu)
}

class CartCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    let disposeBag = DisposeBag()
    var menu: Menu? {
        didSet {
            guard let menu = menu else { return }
            if let imageUrl = URL(string: menu.image) {
                imageView.kf.setImage(with: imageUrl)
            }
            nameLabel.text = menu.name
            priceLabel.text = "USD \(menu.price)"
        }
    }
    var delegate: CartCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.kf.indicatorType = .activity
        button.rx.tap.subscribe(onNext: { () in
            guard let menu = self.menu else { return }
            self.delegate?.removeItem(menu: menu)
        }).disposed(by: disposeBag)
    }

}
