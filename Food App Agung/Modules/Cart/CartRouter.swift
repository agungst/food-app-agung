//
//  CartRouter.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 08/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

class CartRouter: CartRouterProtocol {
    
    weak var viewController: CartViewController?
    
    var delegate: MenuPresenterProtocol?

     // MARK: RouterProtocol
    func backToMenu(menus: [Menu]) {
        delegate?.updateCart(menus: menus)
        viewController?.dismiss(animated: true)
    }

}
