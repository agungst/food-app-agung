//
//  CartContract.swift
//  Food App Agung
//
//  Created by Agung Setiawan on 08/11/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

protocol CartRouterProtocol: class {
    func backToMenu(menus: [Menu])
}

protocol CartPresenterProtocol: class {
    
    var menus: [Menu] { get set }
    
    func viewDidLoad()
    func backToMenu()

}

protocol CartInteractorOutputProtocol: class {
    
    func requestDidSuccess()
    func requestDidFailed(message: String)
}

protocol CartInteractorInputProtocol: class {
    
    var output: CartInteractorOutputProtocol? { get set }

    func fetchData()
    
}

protocol CartViewProtocol: class {
    
    // Presenter -> ViewController
    func populateData()
    func showProgressDialog(show: Bool)
    func showAlertMessage(title: String, message: String)
    func showErrorMessage(message: String)
    
}
